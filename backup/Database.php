<?php

class Database {
	
	public static function loadSettingsBackupFromDatabase($name) {
		$qSelect = mysql_query("SELECT * FROM SETTINGS_BACKUP WHERE NAME='" . $name . "';");
		$sb = null;
		while ($rSelect = mysql_fetch_assoc($qSelect)) {
			$name = 'SettingsBackup' . $name;
			$sb = new $name($rSelect['ACTIVE'], $rSelect['PERIOD'], $rSelect['DAY'],
					$rSelect['TIME'], $rSelect['DATE_FROM'], $rSelect['DATE_TO'], $rSelect['COMPRESSION']);
		}
		return $sb;
	}
	
	public static function saveSettingsBackupToDatabase($name, SettingsBackup $sb) { 
		$sql = "UPDATE SETTINGS_BACKUP SET 
				ACTIVE='" . $sb->getActive() . "', 
				PERIOD='" . $sb->getPeriod() . "', 
				DAY='" . $sb->getDay() . "', 
				TIME='" . $sb->getTime() . "', 
				DATE_FROM='" . $sb->getDateFrom() . "', 
				DATE_TO='" . $sb->getDateTo() . "', 
				COMPRESSION='" . $sb->getCompression() . "' 
						WHERE NAME='" . $name . "'; ";
		
		mysql_query($sql) or die("Nastala chyba p�i vykon�v�n� MySQL p��kazu.<br>" . mysql_error());
	}
	
	
	
	public static function loadSettingsAccessFromDatabase($name) {
		$qSelect = mysql_query("SELECT * FROM SETTINGS_ACCESS WHERE NAME='" . $name . "';");
		$sa = null;
		while ($rSelect = mysql_fetch_assoc($qSelect)) {
			$name = 'SettingsAccess' . $name;
			
			if ($name == "SettingsAccessWebApp") $sa = new $name($rSelect['HOST'], $rSelect['PORT'], $rSelect['USERNAME'], $rSelect['PASSWORD']);
				else $sa = new $name($rSelect['HOST'], $rSelect['DATABASE_NAME'], $rSelect['USERNAME'], $rSelect['PASSWORD']);
		}
		return $sa;
	}	
	
	public static function saveSettingsAccessToDatabase($name, SettingsAccess $sa) {
		if ($name == "WebApp") {
			$sql = "UPDATE SETTINGS_ACCESS SET
						HOST='" . $sa->getHost() . "',
						PORT='" . $sa->getPort() . "',
						USERNAME='" . $sa->getUsername() . "',
						PASSWORD='" . $sa->getPassword() . "'
								WHERE NAME='" . $name . "'; ";
		}	else {
				$sql = "UPDATE SETTINGS_ACCESS SET
							HOST='" . $sa->getHost() . "',
							DATABASE_NAME='" . $sa->getDatabaseName() . "',
							USERNAME='" . $sa->getUsername() . "',
							PASSWORD='" . $sa->getPassword() . "'
									WHERE NAME='" . $name . "'; ";			
		}
	
		mysql_query($sql) or die("Nastala chyba p�i vykon�v�n� MySQL p��kazu.<br>" . mysql_error());
	}	
	
	
	
	public static function connect() {
		if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1') $connect = @mysql_connect("localhost","root","");
			else $connect = mysql_connect("","","");
		mysql_select_db('backup', $connect);
	}
	
	public static function disconnect() {
		mysql_close();
	}
	
}

?>