<?php

class SettingsAccessDatabase extends SettingsAccess {

	private $databaseName;
	
	public function __construct($host, $databaseName, $username, $password) {
		parent::__construct($host, $username, $password);
		$this->databaseName = $databaseName;
	}
	
	public function getDatabaseName() {
		return $this->databaseName;
	}
	public function setDatabaseName($databaseName) {
		$this->databaseName = $databaseName;
		return $this;
	}
	
}

?>