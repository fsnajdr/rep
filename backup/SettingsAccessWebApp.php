<?php

class SettingsAccessWebApp extends SettingsAccess {
	
	private $port;
	
	public function __construct($host, $port, $username, $password) {
		parent::__construct($host, $username, $password);
		$this->port = $port;
	}	
	
	public function getPort() {
		return $this->port;
	}
	public function setPort($port) {
		$this->port = $port;
		return $this;
	}
	
}

?>