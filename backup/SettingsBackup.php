<?php

abstract class SettingsBackup {

	private $active; 		// is backup process active?
	private $period; 		// Daily / Weekly / Monthly
	private $day; 			// specifies $period 
	private $time;			// start time
	private $dateFrom;		// limit for backup process
	private $dateTo;		// limit for backup process
	private $compression;	// backup compression  
	
	public function __construct($active, $period, $day, $time, $dateFrom, $dateTo, $compression) {
		$this->active = $active;
		$this->period = $period;
		$this->day = $day;
		$this->time = $time;
		$this->dateFrom = $dateFrom;
		$this->dateTo = $dateTo;
		$this->compression = $compression;
	}
			
	public function getActive() {
		return $this->active;
	}
	public function setActive($active) {
		$this->active = $active;
		return $this;
	}
	public function getPeriod() {
		return $this->period;
	}
	public function setPeriod($period) {
		$this->period = $period;
		return $this;
	}
	public function getDay() {
		return $this->day;
	}
	public function setDay($day) {
		$this->day = $day;
		return $this;
	}
	public function getTime() {
		return $this->time;
	}
	public function setTime($time) {
		$this->time = $time;
		return $this;
	}
	public function getDateFrom() {
		return $this->dateFrom;
	}
	public function setDateFrom($dateFrom) {
		$this->dateFrom = $dateFrom;
		return $this;
	}
	public function getDateTo() {
		return $this->dateTo;
	}
	public function setDateTo($dateTo) {
		$this->dateTo = $dateTo;
		return $this;
	}
	public function getCompression() {
		return $this->compression;
	}
	public function setCompression($compression) {
		$this->compression = $compression;
		return $this;
	}
		
}

?>