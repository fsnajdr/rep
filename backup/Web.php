<?php

class Web {
	
	
	public static function addForm($name, $method, $action) {
		echo '<form name="' . $name . '" id="' . $name . '" method="' . $method . '" action="' . $action . '" style="float: left;">';
	}
	
	public static function addEndTag($tag) {
		echo '</'  . $tag . '>';
	}
	
	public static function addTable() {
		echo '<table>';
	}
		
	public static function addTrTh($title) {
		echo '<tr><th colspan="2" style="text-align: left;">' . $title . '</th></tr>';
	}
	
	public static function addTr($description, $type, $name, $value, $class, $attritubes = "") {
		echo '<tr><td>' . $description . '</td><td>';
		
		if ($type == 'checkbox') {
			if ($value == 'true') $attritubes .= ' checked="checked"';
				else $value = 'true'; //
		}
		
		if ($description == 'Period') {
			switch ($value) {
				case 'D':
					$value = 'Daily';
					break;
				case 'W':
					$value = 'Weekly';
					break;
				case 'M':
					$value = 'Monthly';
					break;
				default:
					$value = 'null';
					break;
			} 
		}
		self::addInput($type, $name, $value, $class, $attritubes);
		echo '</td></tr>';
	}
	
	public static function addInput($type, $name, $value, $class, $attritubes = "") {
		echo '<input type="' . $type . '" name="' . $name . '" id="' . $name . '" value="' . $value . '" class="' . $class . '"'. $attritubes . '>';
	}
	
	public static function addMsg($msg, $color = '') {
		echo '<div id="msg"';
		if ($color != '') echo ' style="background-color: ' . $color . '"';
		echo '>' . $msg . '</div>';
	}
	
}

?>