<!DOCTYPE html>
<html>
<head>
<style>
body {margin: 50px;} 
div#msg {background-color: lightblue; color: white; padding: 20px; margin-bottom: 30px;}
form {margin-right: 100px;}
</style>
</head>

<body>

<?php

function __autoload($class) {
	require_once $class . '.php';
}

Database::connect();

$sad = Database::loadSettingsAccessFromDatabase("Database");
$saw = Database::loadSettingsAccessFromDatabase("WebApp");

if (isset($_POST['host'])) {
	$sad->setHost($_POST['host']);
	$sad->setDatabaseName($_POST['databaseName']);
	$sad->setUsername($_POST['username']);
	$sad->setPassword($_POST['password']);

	Database::saveSettingsAccessToDatabase("Database", $sad);
	Web::addMsg("Database settings saved", "green");
}

if (isset($_POST['host2'])) {
	$saw->setHost($_POST['host2']);
	$saw->setPort($_POST['port2']);
	$saw->setUsername($_POST['username2']);
	$saw->setPassword($_POST['password2']);

	Database::saveSettingsAccessToDatabase("WebApp", $saw);
	Web::addMsg("WebApp settings saved", "green");
}


Web::addForm("formDatabase", "POST", "#");
Web::addTable();
Web::addTrTh("MySQL");
Web::addTr("Host", "text", "host", $sad->getHost(), "text", " required");
Web::addTr("Database", "text", "databaseName", $sad->getDatabaseName(), "text", " required");
Web::addTr("Username", "text", "username", $sad->getUsername(), "text", " required");
Web::addTr("Password", "password", "password", $sad->getPassword(), "text", " required");
Web::addTr("&nbsp;", "submit", "bSave", "Save", "button");
Web::addEndTag("table");
Web::addEndTag("form"); 

Web::addForm("formWebApp", "POST", "#");
Web::addTable();
Web::addTrTh("FTP");
Web::addTr("Host", "text", "host2", $saw->getHost(), "text", " required");
Web::addTr("Port", "number", "port2", $saw->getPort(), "number");
Web::addTr("Username", "text", "username2", $saw->getUsername(), "text", " required");
Web::addTr("Password", "password", "password2", $saw->getPassword(), "text", " required");
Web::addTr("&nbsp;", "submit", "bSave", "Save", "button");
Web::addEndTag("table");
Web::addEndTag("form");

Database::disconnect();

?>
</body>
</html>