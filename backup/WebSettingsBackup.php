<!DOCTYPE html>
<html>
<head>
<style>
body {margin: 50px;} 
div#msg {background-color: lightblue; color: white; padding: 20px; margin-bottom: 30px;}
form {margin-right: 100px;}
</style>
</head>

<body>

<?php
/* 
TODO:
	exceptions, input validation
	messages
	$period >>> <select><options={D, W, M}></select> 
	$day >>> conditioned:	if ($period == 'W') $day = {'MON', ... , 'SUN'}
							if ($period == 'M') $day = {1, 2, ... , 31}
	would be nice: components: calendar, time
 */

function __autoload($class) {
	require_once $class . '.php';
}

Database::connect();

$sbd = Database::loadSettingsBackupFromDatabase("Database");
$sbw = Database::loadSettingsBackupFromDatabase("WebApp");

if (isset($_POST['day'])) {
	
	if (isset($_POST['active'])) $active = 'true';
		else $active = 'false';
	$sbd->setActive($active);
	
	switch ($_POST['period']) {
		case 'Daily':
			$period = 'D';
			break;
		case 'Weekly':
			$period = 'W';
			break;
		case 'Monthly':
			$period = 'M';
			break;
		default:
			$period = 'null';
			break;
	}	
	$sbd->setPeriod($period);
	
	$sbd->setDay($_POST['day']);
	$sbd->setTime($_POST['time']);
	$sbd->setDateFrom($_POST['dateFrom']);
	$sbd->setDateTo($_POST['dateTo']);

	if (isset($_POST['compression'])) $compression = 'true';
	  else $compression = 'false'; 
	$sbd->setCompression($compression);
	
	Database::saveSettingsBackupToDatabase("Database", $sbd);
	Web::addMsg("Database settings saved", "green");
}

if (isset($_POST['day2'])) {

	if (isset($_POST['active2'])) $active = 'true';
	else $active = 'false';
	$sbw->setActive($active);

	switch ($_POST['period2']) {
		case 'Daily':
			$period = 'D';
			break;
		case 'Weekly':
			$period = 'W';
			break;
		case 'Monthly':
			$period = 'M';
			break;
		default:
			$period = 'null';
			break;
	}
	$sbw->setPeriod($period);

	$sbw->setDay($_POST['day2']);
	$sbw->setTime($_POST['time2']);
	$sbw->setDateFrom($_POST['dateFrom2']);
	$sbw->setDateTo($_POST['dateTo2']);

	if (isset($_POST['compression2'])) $compression = 'true';
	else $compression = 'false';
	$sbw->setCompression($compression);

	Database::saveSettingsBackupToDatabase("WebApp", $sbw);
	Web::addMsg("WebApp settings saved", "green");
}


Web::addForm("formDatabase", "POST", "#");
Web::addTable();
Web::addTrTh("Database");
Web::addTr("Active", "checkbox", "active", $sbd->getActive(), "checkbox");
Web::addTr("Period", "text", "period", $sbd->getPeriod(), "text", " required");
Web::addTr("Day", "text", "day", $sbd->getDay(), "text", " required");
Web::addTr("Time", "text", "time", $sbd->getTime(), "text", " required");
Web::addTr("Start Date", "text", "dateFrom", $sbd->getDateFrom(), "text", " required");
Web::addTr("End Date", "text", "dateTo", $sbd->getDateTo(), "text", " required");
Web::addTr("Compression", "checkbox", "compression", $sbd->getCompression(), "checkbox");
Web::addTr("&nbsp;", "submit", "bSave", "Save", "button");
Web::addEndTag("table");
Web::addEndTag("form");

Web::addForm("formWebApp", "POST", "#");
Web::addTable();
Web::addTrTh("WebApp");
Web::addTr("Active", "checkbox", "active2", $sbw->getActive(), "checkbox");
Web::addTr("Period", "text", "period2", $sbw->getPeriod(), "text", " required");
Web::addTr("Day", "text", "day2", $sbw->getDay(), "text", " required");
Web::addTr("Time", "text", "time2", $sbw->getTime(), "text", " required");
Web::addTr("Start Date", "text", "dateFrom2", $sbw->getDateFrom(), "text", " required");
Web::addTr("End Date", "text", "dateTo2", $sbw->getDateTo(), "text", " required");
Web::addTr("Compression", "checkbox", "compression2", $sbw->getCompression(), "checkbox");
Web::addTr("&nbsp;", "submit", "bSave", "Save", "button");
Web::addEndTag("table");
Web::addEndTag("form");

Database::disconnect();

?>
</body>
</html>