-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Úte 17. úno 2015, 21:18
-- Verze serveru: 5.6.15-log
-- Verze PHP: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáze: `backup`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `settings_access`
--

CREATE TABLE IF NOT EXISTS `settings_access` (
  `NAME` varchar(8) COLLATE cp1250_czech_cs NOT NULL,
  `HOST` varchar(32) COLLATE cp1250_czech_cs NOT NULL,
  `PORT` int(3) NOT NULL,
  `USERNAME` varchar(32) COLLATE cp1250_czech_cs NOT NULL,
  `PASSWORD` varchar(32) COLLATE cp1250_czech_cs NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1250 COLLATE=cp1250_czech_cs;

--
-- Vypisuji data pro tabulku `settings_access`
--

INSERT INTO `settings_access` (`NAME`, `HOST`, `PORT`, `USERNAME`, `PASSWORD`) VALUES
('WebApp', 'arch1.wz.cz', 21, 'arch1.wz.cz', 'adm:n3269'),
('Database', 'sql', 0, 'username', 'pass');

-- --------------------------------------------------------

--
-- Struktura tabulky `settings_backup`
--

CREATE TABLE IF NOT EXISTS `settings_backup` (
  `NAME` varchar(8) COLLATE cp1250_czech_cs NOT NULL,
  `ACTIVE` enum('true','false') COLLATE cp1250_czech_cs NOT NULL,
  `PERIOD` enum('D','W','M') COLLATE cp1250_czech_cs NOT NULL,
  `DAY` varchar(3) COLLATE cp1250_czech_cs NOT NULL,
  `TIME` time NOT NULL,
  `DATE_FROM` date NOT NULL,
  `DATE_TO` date NOT NULL,
  `COMPRESSION` enum('true','false') COLLATE cp1250_czech_cs NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1250 COLLATE=cp1250_czech_cs;

--
-- Vypisuji data pro tabulku `settings_backup`
--

INSERT INTO `settings_backup` (`NAME`, `ACTIVE`, `PERIOD`, `DAY`, `TIME`, `DATE_FROM`, `DATE_TO`, `COMPRESSION`) VALUES
('WebApp', 'false', 'M', '1', '12:00:00', '2016-01-01', '2016-12-31', 'true'),
('Database', 'true', 'W', 'MON', '18:00:00', '2015-02-01', '2015-12-31', 'true');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
